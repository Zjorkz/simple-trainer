# Imports
import tkinter
from tkinter import ttk

#/////////////////////////////////////////

# Window setup

main_window = tkinter.Tk() # Create main window of programme

main_window.title('Simple Trainer') # Title of main_window
main_window.geometry('550x530') # Dimensions for main_window
main_window.wm_iconbitmap('favicon.ico') # Set icon for main_window

tabs = ttk.Notebook(main_window)

#/////////////////////////////////////////

# Create tabs

home = ttk.Frame(tabs)
botting = ttk.Frame(tabs)
hacks = ttk.Frame(tabs)
item_filter = ttk.Frame(tabs)


#/////////////////////////////////////////

# Add tabs for main_window

tabs.add(home, text='Home')
tabs.add(botting, text='Botting')
tabs.add(hacks, text='Hacks')
tabs.add(item_filter, text='Item Filter')

tabs.pack(expand=1, fill='both')

#/////////////////////////////////////////

# Menu setup




# Constants
#def callback() :
    # open C: - Change text to = success only when valid file is chosen

#/////////////////////////////////////////

# BOTTING TAB

#/////////////////////////////////////////

#/////////////////////////////////////////

# AUTO LOGIN

#/////////////////////////////////////////

# AUTO LOGIN Frame

autologin_frame = tkinter.LabelFrame(botting, text='Auto Login')
autologin_frame.place(x=6,height=240, width=200)

# Enable

autologin_enable_cb = tkinter.IntVar()
E = tkinter.Checkbutton(autologin_frame, text='Enable', variable=autologin_enable_cb)

E.place(x=2,y=3)

# Hide Info

hideinfo_cb = tkinter.IntVar()
HI = tkinter.Checkbutton(autologin_frame, text='Hide Info', variable=hideinfo_cb)

HI.place(x=70,y=3)

# World Select
worldselect_om = tkinter.StringVar(autologin_frame)
worldselect_om.set('World')

worldselect_options = tkinter.OptionMenu(autologin_frame,worldselect_om,'Scania','Bera', 'Broa', 'Windia',
                                         'Khaini', 'Bellocan', 'Mardia','Kradia', 'Yellonde', 'Demethos',
                                         'Galicia', 'El Nido', 'Zenith', 'Arcania', 'Chaos', 'Nova',
                                         'Renegades','Reboot'   )
worldselect_options.place(x=2, y=30)

# Channel Select

channelselect_om = tkinter.StringVar(autologin_frame)
channelselect_om.set('Channel')

channelselect_options = tkinter.OptionMenu(autologin_frame,channelselect_om,'Random', '1', '2', '3', '4', '5'
                                            , '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17',
                                            '18', '19', '20')

channelselect_options.place(x=85,y=30)

# Delay for Auto Login

delay_lbl = tkinter.Label(autologin_frame, text='Delay')
delay_lbl.place(x=2,y=69)

delay_sb = tkinter.Spinbox(autologin_frame, width=8, from_=0, to=120)
delay_sb.place(x=45,y=70)

# Invis

invis_cb = tkinter.IntVar()
IV = tkinter.Checkbutton(autologin_frame, text='Invisble', variable=invis_cb)

IV.place(x=115,y=68)

# Username Entry

username_ety = tkinter.Entry(autologin_frame)
username_ety.place(x=2,y=100,width=150)
username_ety.insert(0,'Username')

# Password Entry

password_ety = tkinter.Entry(autologin_frame)
password_ety.place(x=2,y=128,width=150)
password_ety.insert(0,'Password')

# PIC Entry

pic_ety = tkinter.Entry(autologin_frame)
pic_ety.place(x=2,y=156,width=150)
pic_ety.insert(0, 'PIC')

# Character

character_lbl = tkinter.Label(autologin_frame, text='Character')
character_lbl.place(x=2,y=185)

character_sb = tkinter.Spinbox(autologin_frame, width=8, from_=0, to=36)
character_sb.place(x=70,y=186)

#/////////////////////////////////////////

# AUTO CC

#/////////////////////////////////////////

# AUTO CC Frame

autocc_frame = tkinter.LabelFrame(botting, text='Auto CC')
autocc_frame.place(x=6,y=250,height=240, width=200)

#/////////////////////////////////////////

# Players

players_cb = tkinter.IntVar()
Players = tkinter.Checkbutton(autocc_frame, text='Players', variable=players_cb)

Players.place(x=2,y=3)

players_sb = tkinter.Spinbox(autocc_frame, width=8, from_=0, to=100)
players_sb.place(x=80,y=5)

# Interval

interval_cb = tkinter.IntVar()
interval = tkinter.Checkbutton(autocc_frame, text='Interval', variable=interval_cb)

interval.place(x=2, y=30)

intervall_sb = tkinter.Spinbox(autocc_frame, width=8, from_=1, to=120)
intervall_sb.place(x=80, y=32)

# Mode

mode_lbl = tkinter.Label(autocc_frame, text='Mode')
mode_lbl.place(x=2,y=69)

mode_om = tkinter.StringVar()
mode_om.set('Linear')

mode_select = tkinter.OptionMenu(autocc_frame,mode_om,'Linear', 'Random')

mode_select.place(x=46,y=63)

# Channels

channel1_cb = tkinter.IntVar()
channel1_cb.set(1)
c1 = tkinter.Checkbutton(autocc_frame, text='1', variable=channel1_cb,command=channel1_cb,onvalue=1,offvalue=0)

c1.place(x=2, y=100)

channel2_cb = tkinter.IntVar()
channel2_cb.set(1)
c2 = tkinter.Checkbutton(autocc_frame, text='2', variable=channel2_cb,command=channel2_cb,onvalue=1,offvalue=0)

c2.place(x=40,y=100)

channel3_cb = tkinter.IntVar()
channel3_cb.set(1)
c3 = tkinter.Checkbutton(autocc_frame, text='3', variable=channel3_cb,command=channel3_cb,onvalue=1,offvalue=0)

c3.place(x=78,y=100)

channel4_cb = tkinter.IntVar()
channel4_cb.set(1)
c4 = tkinter.Checkbutton(autocc_frame, text='4', variable=channel4_cb,command=channel4_cb,onvalue=1,offvalue=0)

c4.place(x=116,y=100)

channel5_cb = tkinter.IntVar()
channel5_cb.set(1)
c5 = tkinter.Checkbutton(autocc_frame, text='5', variable=channel5_cb,command=channel5_cb,onvalue=1,offvalue=0)

c5.place(x=154,y=100)

channel6_cb = tkinter.IntVar()
channel6_cb.set(1)
c6 = tkinter.Checkbutton(autocc_frame, text='6', variable=channel6_cb,command=channel6_cb,onvalue=1,offvalue=0)

c6.place(x=2, y=125)

channel7_cb = tkinter.IntVar()
channel7_cb.set(1)
c7 = tkinter.Checkbutton(autocc_frame, text='7', variable=channel7_cb,command=channel7_cb,onvalue=1,offvalue=0)

c7.place(x=40,y=125)

channel8_cb = tkinter.IntVar()
channel8_cb.set(1)
c8 = tkinter.Checkbutton(autocc_frame, text='8', variable=channel8_cb,command=channel8_cb,onvalue=1,offvalue=0)

c8.place(x=78,y=125)

channel9_cb = tkinter.IntVar()
channel9_cb.set(1)
c9 = tkinter.Checkbutton(autocc_frame, text='9', variable=channel9_cb,command=channel9_cb,onvalue=1,offvalue=0)

c9.place(x=116,y=125)

channel10_cb = tkinter.IntVar()
channel10_cb.set(1)
c10 = tkinter.Checkbutton(autocc_frame, text='10', variable=channel10_cb,command=channel10_cb,onvalue=1,offvalue=0)

c10.place(x=154,y=125)

channel11_cb = tkinter.IntVar()
channel11_cb.set(1)
c11 = tkinter.Checkbutton(autocc_frame, text='11', variable=channel11_cb,command=channel11_cb,onvalue=1,offvalue=0)

c11.place(x=2,y=150)

channel12_cb = tkinter.IntVar()
channel12_cb.set(1)
c12 = tkinter.Checkbutton(autocc_frame, text='12', variable=channel12_cb,command=channel12_cb,onvalue=1,offvalue=0)

c12.place(x=40,y=150)

channel13_cb = tkinter.IntVar()
channel13_cb.set(1)
c13 = tkinter.Checkbutton(autocc_frame, text='13', variable=channel13_cb,command=channel13_cb,onvalue=1,offvalue=0)

c13.place(x=78,y=150)

channel14_cb = tkinter.IntVar()
channel14_cb.set(1)
c14 = tkinter.Checkbutton(autocc_frame, text='14', variable=channel14_cb,command=channel14_cb,onvalue=1,offvalue=0)

c14.place(x=116,y=150)

channel15_cb = tkinter.IntVar()
channel15_cb.set(1)
c15 = tkinter.Checkbutton(autocc_frame, text='15', variable=channel15_cb,command=channel15_cb,onvalue=1,offvalue=0)

c15.place(x=154,y=150)

channel16_cb = tkinter.IntVar()
channel16_cb.set(1)
c16 = tkinter.Checkbutton(autocc_frame, text='16', variable=channel16_cb,command=channel16_cb,onvalue=1,offvalue=0)

c16.place(x=2,y=175)

channel17_cb = tkinter.IntVar()
channel17_cb.set(1)
c17 = tkinter.Checkbutton(autocc_frame, text='17', variable=channel17_cb,command=channel17_cb,onvalue=1,offvalue=0)

c17.place(x=40,y=175)

channel18_cb = tkinter.IntVar()
channel18_cb.set(1)
c18 = tkinter.Checkbutton(autocc_frame, text='18', variable=channel18_cb,command=channel18_cb,onvalue=1,offvalue=0)

c18.place(x=78,y=175)

channel19_cb = tkinter.IntVar()
channel19_cb.set(1)
c19 = tkinter.Checkbutton(autocc_frame, text='19', variable=channel19_cb,command=channel19_cb,onvalue=1,offvalue=0)

c19.place(x=116,y=175)

channel20_cb = tkinter.IntVar()
channel20_cb.set(1)
c20 = tkinter.Checkbutton(autocc_frame, text='20', variable=channel20_cb,command=channel20_cb,onvalue=1,offvalue=0)

c20.place(x=154,y=175)

#/////////////////////////////////////////

# MACROS

#/////////////////////////////////////////

# Macros Frame

macros_frame = tkinter.LabelFrame(botting, text='Macros')
macros_frame.place(x=225,height=400, width=300)

#/////////////////////////////////////////

# Auto Macro's

# Auto Hp

autohp_cb = tkinter.IntVar()
HP = tkinter.Checkbutton(macros_frame, text='Auto Hp', variable=autohp_cb)

HP.place(x=2,y=3)

autohp_sb = tkinter.Spinbox(macros_frame, width=8, from_=50, to=99999)
autohp_sb.place(x=95,y=5)

autohp_descrip = tkinter.Label(macros_frame, text='( Page Up )')
autohp_descrip.place(x=160,y=4)

# Auto Mp

automp_cb = tkinter.IntVar()
MP = tkinter.Checkbutton(macros_frame, text='Auto Mp', variable=automp_cb)

MP.place(x=2,y=28)

automp_sb = tkinter.Spinbox(macros_frame, width=8, from_=50, to=99999)
automp_sb.place(x=95,y=30)

automp_descrip = tkinter.Label(macros_frame, text='( Page Down )')
automp_descrip.place(x=160, y=29)

# Auto Feed

autofeed_cb = tkinter.IntVar()
FEED = tkinter.Checkbutton(macros_frame, text='Auto Feed', variable=autofeed_cb)

FEED.place(x=2,y=53)

autofeed_sb = tkinter.Spinbox(macros_frame, width=8, from_=50, to=100)
autofeed_sb.place(x=95,y=55)

autofeed_descrip = tkinter.Label(macros_frame, text='( ~ )')
autofeed_descrip.place(x=160,y=54)

# Auto Loot

autoloot_cb = tkinter.IntVar()
LOOT = tkinter.Checkbutton(macros_frame, text='Auto Loot', variable=autoloot_cb)

LOOT.place(x=2,y=78)

autoloot_sb = tkinter.Spinbox(macros_frame, width=8, from_=1, to=180)
autoloot_sb.place(x=95,y=80)

autoloot_descrip = tkinter.Label(macros_frame, text='( Z )')
autoloot_descrip.place(x=160,y=79)

# Auto Attack

autoattack_cb = tkinter.IntVar()
ATTACK = tkinter.Checkbutton(macros_frame, text='Auto Attack', variable=autoattack_cb)

ATTACK.place(x=2,y=103)

autoattack_sb = tkinter.Spinbox(macros_frame, width=8, from_=0.5, to=180)
autoattack_sb.place(x=95,y=105)

autoattack_descrip = tkinter.Label(macros_frame, text='( Ctrl )')
autoattack_descrip.place(x=160,y=104)

# Auto Attack Hold / Spam

aa_hold = tkinter.IntVar()
aa_spam = tkinter.IntVar()

aa_spam.set(1)

HOLD = tkinter.Checkbutton(macros_frame, text='Hold', variable=aa_hold)
SPAM = tkinter.Checkbutton(macros_frame, text='Spam', variable=aa_spam, command=aa_spam, onvalue=1, offvalue=0)

HOLD.place(x=2,y=130)
SPAM.place(x=60,y=130)

# Make it so when one is checked the other is unchecked

# Key Macros - Min value = 2 seconds

# Insert

insert_cb = tkinter.IntVar()
INSERT = tkinter.Checkbutton(macros_frame, text='Insert', variable=insert_cb)
INSERT.place(x=2,y=180)

insert_sb = tkinter.Spinbox(macros_frame, width=8, from_=2, to=1320)
insert_sb.place(x=62,y=183)

# Home

home_cb = tkinter.IntVar()
HOME = tkinter.Checkbutton(macros_frame, text='Home', variable=home_cb)
HOME.place(x=135,y=180)

home_sb = tkinter.Spinbox(macros_frame, width=8, from_=2, to=1320)
home_sb.place(x=198,y=183)


# Delete

delete_cb = tkinter.IntVar()
DELETE = tkinter.Checkbutton(macros_frame, text='Delete', variable=delete_cb)
DELETE.place(x=2,y=205)

delete_sb = tkinter.Spinbox(macros_frame, width=8, from_=2, to=1320)
delete_sb.place(x=62,y=208)


# End

end_cb = tkinter.IntVar()
END = tkinter.Checkbutton(macros_frame, text='End', variable=end_cb)
END.place(x=135,y=205)

end_sb = tkinter.Spinbox(macros_frame, width=8, from_=2, to=1320)
end_sb.place(x=198,y=208)



# Number Macros - Min Value = 10 Seconds

button_0 = tkinter.IntVar()
BUT0 = tkinter.Checkbutton(macros_frame, text='0', variable=button_0)
BUT0.place(x=2,y=240)

button0_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button0_sb.place(x=62,y=243)


button_1 = tkinter.IntVar()
BUT1 = tkinter.Checkbutton(macros_frame, text='1', variable=button_1)
BUT1.place(x=2,y=265)

button1_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button1_sb.place(x=62,y=268)


button_2 = tkinter.IntVar()
BUT2 = tkinter.Checkbutton(macros_frame, text='2', variable=button_2)
BUT2.place(x=2,y=290)

button2_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button2_sb.place(x=62,y=293)


button_3 = tkinter.IntVar()
BUT3 = tkinter.Checkbutton(macros_frame, text='3', variable=button_3)
BUT3.place(x=2,y=315)

button3_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button3_sb.place(x=62,y=318)


button_4 = tkinter.IntVar()
BUT4 = tkinter.Checkbutton(macros_frame, text='4', variable=button_4)
BUT4.place(x=2,y=340)

button4_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button4_sb.place(x=62,y=343)


button_5 = tkinter.IntVar()
BUT5 = tkinter.Checkbutton(macros_frame, text='5', variable=button_5)
BUT5.place(x=135,y=240)

button5_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button5_sb.place(x=195,y=243)


button_6 = tkinter.IntVar()
BUT6 = tkinter.Checkbutton(macros_frame, text='6', variable=button_6)
BUT6.place(x=135,y=265)

button6_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button6_sb.place(x=195,y=268)


button_7 = tkinter.IntVar()
BUT7 = tkinter.Checkbutton(macros_frame, text='7', variable=button_7)
BUT7.place(x=135,y=290)

button7_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button7_sb.place(x=195,y=293)


button_8 = tkinter.IntVar()
BUT8 = tkinter.Checkbutton(macros_frame, text='8', variable=button_8)
BUT8.place(x=135,y=315)

button8_sb = tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button8_sb.place(x=195,y=318)


button_9 = tkinter.IntVar()
BUT9 = tkinter.Checkbutton(macros_frame, text='9', variable=button_9)
BUT9.place(x=135,y=340)

button9_sb =  tkinter.Spinbox(macros_frame, width=8, from_=10, to=1320)
button9_sb.place(x=195,y=343)

#/////////////////////////////////////////

# Familiar Summon

#/////////////////////////////////////////

# Familiar Summon Frame

familiar_frame = tkinter.LabelFrame(botting, text='Familiar Summon')
familiar_frame.place(x=225,y=420,height=50, width=300)

#/////////////////////////////////////////

# Familiar Summon

familiar_enable = tkinter.IntVar()
FE = tkinter.Checkbutton(familiar_frame, text='Enable', variable=familiar_enable)
FE.place(x=2)

familiar_delay = tkinter.IntVar()
FD = tkinter.Checkbutton(familiar_frame, text='Delay', variable=familiar_delay)
FD.place(x=65)

familiarsummon_sb = tkinter.Spinbox(familiar_frame, width=4, from_=10, to=1320)
familiarsummon_sb.place(x=125,y=3)

familiarsummon_om = tkinter.StringVar(familiar_frame)
familiarsummon_om.set('Wolf Underling')

familiarsummon_options = tkinter.OptionMenu(familiar_frame,familiarsummon_om,'Wolf Underling','Jr. Boogie',
                                            'Eye of Time', 'Big Spider', 'Leprechaun')

familiarsummon_options.place(x=170,y=-4)

#/////////////////////////////////////////

#/////////////////////////////////////////

# HACKS TAB

#/////////////////////////////////////////

# Basic Hacks Frame

basichacks_frame =  tkinter.LabelFrame(hacks, text='Basic Hacks')
basichacks_frame.place(x=6,height=490,width=200)

#/////////////////////////////////////////

# Full God Mode

full_god_mode = tkinter.IntVar()
FGM = tkinter.Checkbutton(basichacks_frame, text='Full God Mode', variable=full_god_mode )

FGM.place(x=2)

# 58 Sec God Mode

god_mode_58 = tkinter.IntVar()
GM58 = tkinter.Checkbutton(basichacks_frame, text='58 Sec God Mode', variable=god_mode_58)

GM58.place(x=2,y=20)

# Air Check

air_check = tkinter.IntVar()
AC = tkinter.Checkbutton(basichacks_frame, text='Air Check', variable=air_check)

AC.place(x=2,y=40)

# Unlimited Attack - Enable by default

unlimited_attack = tkinter.IntVar()
unlimited_attack.set(1)
UA = tkinter.Checkbutton(basichacks_frame, text='Unlimited Attack', variable=unlimited_attack, onvalue=1, offvalue=0)

UA.place(x=2,y=60)

# No Delay FJ

nodelay_fj = tkinter.IntVar()
NDFJ = tkinter.Checkbutton(basichacks_frame, text='No Delay FJ', variable=nodelay_fj)

NDFJ.place(x=2,y=80)

# Tubi

tubi = tkinter.IntVar()
TUBI = tkinter.Checkbutton(basichacks_frame, text='Tubi', variable=tubi)

TUBI.place(x=2,y=100)

# Air Loot

air_loot = tkinter.IntVar()
AL = tkinter.Checkbutton(basichacks_frame, text='Air Loot', variable=air_loot)

AL.place(x=2,y=120)

# No Crusader Codex

no_codex = tkinter.IntVar()
no_codex.set(1)
NCC = tkinter.Checkbutton(basichacks_frame, text='No Crusader Codex', variable=no_codex, onvalue=1, offvalue=0)

NCC.place(x=2,y=140)

# Mob Disarm

mob_disarm = tkinter.IntVar()
MD = tkinter.Checkbutton(basichacks_frame, text='Mob Disarm', variable=mob_disarm)

MD.place(x=2,y=160)

# Unlimited Arrow Platter

unlimited_aplatter = tkinter.IntVar()
UAP = tkinter.Checkbutton(basichacks_frame, text='Unlimited Arrow Platter', variable=unlimited_aplatter)

UAP.place(x=2,y=180)

# Freeze Mobs

freeze_mobs = tkinter.IntVar()
FM = tkinter.Checkbutton(basichacks_frame, text='Freeze Mobs', variable=freeze_mobs)

FM.place(x=2,y=200)

# Aggro

aggro = tkinter.IntVar()
AGR = tkinter.Checkbutton(basichacks_frame, text='Aggro', variable=aggro)

AGR.place(x=2,y=220)

# Generic FMA

generic_fma = tkinter.IntVar()
GFMA = tkinter.Checkbutton(basichacks_frame, text='Generic FMA', variable=generic_fma)

GFMA.place(x=2,y=240)

# Blaze FMA

blaze_fma = tkinter.IntVar()
BFMA = tkinter.Checkbutton(basichacks_frame, text='Blaze Wizard FMA', variable=blaze_fma)

BFMA.place(x=2,y=260)
# When BFMA is ticked enable SIV

# SIV

siv = tkinter.IntVar()
SIV = tkinter.Checkbutton(basichacks_frame, text='SIV', variable=siv)

SIV.place(x=2,y=280)

# Evan Dragon Kami

evan_kami = tkinter.IntVar()
EKAMI = tkinter.Checkbutton(basichacks_frame, text='Evan Dragon Kami', variable=evan_kami)

EKAMI.place(x=2,y=300)

# CPU Hack

cpu_hack = tkinter.IntVar()
CPUH = tkinter.Checkbutton(basichacks_frame, text='CPU Hack', variable=cpu_hack)

CPUH.place(x=2,y=320)

# Fast Mobs

fast_mobs = tkinter.IntVar()
FMBS = tkinter.Checkbutton(basichacks_frame, text='Fast Mobs', variable=fast_mobs)

FMBS.place(x=2,y=340)

# PIC Typer

pic_typer = tkinter.IntVar()
pic_typer.set(1)

PICT = tkinter.Checkbutton(basichacks_frame, text='Pic Typer', variable=pic_typer, onvalue=1, offvalue=0)

PICT.place(x=2,y=360)

# Slide and Attack

slide_attack = tkinter.IntVar()
SLATTK = tkinter.Checkbutton(basichacks_frame, text='Slide and Attack', variable=slide_attack)

SLATTK.place(x=2,y=380)

# Pet Loot

pet_loot = tkinter.IntVar()
PL = tkinter.Checkbutton(basichacks_frame, text='Pet Loot', variable=pet_loot)

PL.place(x=2,y=400)

# View Swears

view_swears = tkinter.IntVar()
view_swears.set(1)
VS = tkinter.Checkbutton(basichacks_frame, text='View Swears', variable=view_swears, onvalue=1, offvalue=0)

VS.place(x=2,y=420)

# No Falling Objects

no_falling = tkinter.IntVar()
NFO = tkinter.Checkbutton(basichacks_frame, text='No Falling Objects', variable=no_falling)

NFO.place(x=2,y=440)

#/////////////////////////////////////////

# Kami Frame

kami_frame = tkinter.LabelFrame(hacks,text='Kami',height=95, width=300 )
kami_frame.place(x=220)

#/////////////////////////////////////////

# Standard Kami

standard_kami = tkinter.IntVar()
SKAMI = tkinter.Checkbutton(kami_frame, text='Standard Kami', variable=standard_kami)

SKAMI.place(x=2)

kamilbl_x = tkinter.Label(kami_frame, text='X:')
kamilbl_x.place(x=110,y=2)

x_sb = tkinter.Spinbox(kami_frame, width=4, from_=0, to=1320)
x_sb.place(x=130,y=3)

kamilbl_y = tkinter.Label(kami_frame, text='Y:')
kamilbl_y.place(x=175,y=2)

y_sb = tkinter.Spinbox(kami_frame, width=4, from_=0, to=1320)
y_sb.place(x=195,y=3)

# Kami Loot

# When kami loot active auto-enable standard kami

kami_loot = tkinter.IntVar()
KAMIL = tkinter.Checkbutton(kami_frame, text='Kami Loot', variable=kami_loot)

KAMIL.place(x=2,y=20)

kamil_sb = tkinter.Spinbox(kami_frame, width=4, from_=0, to=200)
kamil_sb.place(x=85,y=23)

# Auto loot at 10 items

# Mouse Fly

mouse_fly = tkinter.IntVar()
MOUSEF = tkinter.Checkbutton(kami_frame, text='Mouse Fly', variable=mouse_fly)

MOUSEF.place(x=2,y=40)

#/////////////////////////////////////////

# Spawn Control Frame

spawnpoint_frame = tkinter.LabelFrame(hacks, text = "Spawn Control Point", height = 200, width = 300)
spawnpoint_frame.place(x =220, y=100)

#/////////////////////////////////////////

# Spawn Control

spawn_control = tkinter.IntVar()
sc_button = tkinter.Checkbutton(spawnpoint_frame, text="Spawn Control", variable=spawn_control)
sc_button.place(x=2, y=2)

# Locate

locate_button = tkinter.Button(spawnpoint_frame, text="Locate")
locate_button.place(x=120)

# Add

add_button = tkinter.Button(spawnpoint_frame, text="Add")
add_button.place(x=175)

# Map Label

map_label = tkinter.Label(spawnpoint_frame, text='Map:')
map_label.place(x=2, y=29)

# Map Entry

map_entry = tkinter.Entry(spawnpoint_frame, width=14)
map_entry.place(x=35, y=31)

# X:

map_x_sb_name = tkinter.Label(spawnpoint_frame, text="X:")
map_x_sb_name.place(x=130, y=29)

# X: Spinbox

map_x_sb = tkinter.Spinbox(spawnpoint_frame, width=4, from_=0, to=1000)
map_x_sb.place(x=145, y=30)

# Y:

map_y_sb_name = tkinter.Label(spawnpoint_frame, text="Y:")
map_y_sb_name.place(x=190, y=29)

#Y: Spinbox

map_y_sb = tkinter.Spinbox(spawnpoint_frame, width=4, from_=0, to=1000)
map_y_sb.place(x=205, y=30)

# Delete

delete_button_var = tkinter.IntVar()
delete_button = tkinter.Button(spawnpoint_frame, text="Delete", command=delete_button_var)
delete_button.place(x=247, y=26)

# Map Coordinates Frame

map_coords_frame = tkinter.LabelFrame(hacks, height=120, width=290)
map_coords_frame.place(x =225, y =175)

# Map.ID Label

map_id_label = tkinter.Label(map_coords_frame, text='Map ID')
map_id_label.place(x = 2, y = 2)

# Char.X Label

char_x_id_label = tkinter.Label(map_coords_frame, text='Char.X')
char_x_id_label.place(x = 52, y = 2)

# Char.Y Label

char_y_id_label = tkinter.Label(map_coords_frame, text='Char.Y')
char_y_id_label.place(x = 102, y = 2)

#/////////////////////////////////////////

# ITEM FILTER TAB

#/////////////////////////////////////////

# Item Filter

#/////////////////////////////////////////

# Filter Options Frame

filteroptions_frame = tkinter.LabelFrame(item_filter, text='Filter Options', height=50, width=340)
filteroptions_frame.place(x=2)

#/////////////////////////////////////////

# Enable

enable_cb = tkinter.IntVar()
ENABLE = tkinter.Checkbutton(filteroptions_frame, text='Enable', variable = enable_cb)
ENABLE.place(x=2)


# Accept / Reject Option Menu
accrej_om = tkinter.StringVar()
accrej_om.set('Accept List')

acceptreject_select = tkinter.OptionMenu(filteroptions_frame,accrej_om,'Accept List', 'Reject List')
acceptreject_select.place(x=70,y=-4)

mesolimit_lbl = tkinter.Label(filteroptions_frame, text='Meso Limit:')
mesolimit_lbl.place(x=180,y=1)

mesolimit_sb = tkinter.Spinbox(filteroptions_frame, width=4, from_=0, to=300000)
mesolimit_sb.place(x=250,y=2)

#/////////////////////////////////////////

# Add Item Label Frame

additem_frame = tkinter.LabelFrame(item_filter, text='Add Item', height=205, width=280)
additem_frame.place(x=2,y=55)

# Search

search_ety = tkinter.Entry(additem_frame, width=18)
search_ety.insert(0, 'Search Item')
search_ety.place(x=2)

# Item ID:

itemid_ety = tkinter.Entry(additem_frame, width=14)
itemid_ety.insert(0,'Item ID')
itemid_ety.place(x=120)

# Add Button

addbutton_button = tkinter.Button(additem_frame, text='Add')
addbutton_button.place(x=220,y=-4)

# Secondary Frame

secondary_frame = tkinter.LabelFrame(additem_frame, height=150,width=272)
secondary_frame.place(x=2,y=33)

# Id

id_secondaryframelbl = tkinter.Label(secondary_frame, text='ID')
id_secondaryframelbl.place(x=2)

# Item description

item_secondaryframelbl = tkinter.Label(secondary_frame, text='Item Description')
item_secondaryframelbl.place(x=114)

# Slider

slider_var = tkinter.DoubleVar()
slider = tkinter.Scale(secondary_frame, variable=slider_var)
slider.place(x=222, height=146)

#/////////////////////////////////////////

main_window.mainloop()  # Draw the window and start the programme